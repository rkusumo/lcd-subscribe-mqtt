/* lcd 16x2 displaying message from serial monitor, the next step is susbstitute
the serial monitor with payload from mqtt subscribed messages*/

/*-----( Import needed libraries )-----*/
#include <Wire.h>  // Comes with Arduino IDE
#include <LiquidCrystal_I2C.h>
//mqtt, sim900 libraries
#include <PubSubClient.h>
#include <SoftwareSerial.h>
#include <SIM900Client.h>

/*-----( Declare Constants )-----*/
//none

/*-----( Declare objects )-----*/
// set the LCD address to 0x27 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
//mqtt & sim900 objects
SIM900Client client(4, 3, 6);
byte server[] = { 128, 199, 252, 139 };
SoftwareSerial gsm(4,3);
PubSubClient mqtt(server, 1883, callback, client);

/*-----( Declare Variables )-----*/
//mqtt & sim900 variables
unsigned long timedelay = millis();
bool awal = true;
char topik[50];
int onModulePin = 6;
static char imei[20];

void setup() {  /*----( SETUP: RUNS ONCE )----*/
  Serial.begin(9600);  // Used to type in characters
  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight
  // ------- Quick 3 blinks of backlight  -------------
  for(int i = 0; i< 3; i++) {
    lcd.backlight();
    delay(250);
    lcd.noBacklight();
    delay(250);
  }
  lcd.backlight(); // finish with backlight on
  //-------- Write characters on the display ------------------
  // NOTE: Cursor Position: (CHAR, LINE) start at 0
  lcd.setCursor(0,0); //Start at character 4 on line 0
  lcd.print("LCD");
  istirahat(2000);
  lcd.setCursor(0,1);
  lcd.print("MQTT-SBSCRBR");
  istirahat(2000);
  // Wait and then tell user they can start the Serial Monitor and type in characters to
  // Display. (Set Serial Monitor option to "No Line Ending")
  lcd.clear();
  lcd.setCursor(0,0); //Start at character 0 on line 0
  lcd.print("connecting to");
  lcd.setCursor(0,1);
  lcd.print("mqtt broker...");
  //starting to polling imei and connecting to broker
  gsm.begin(9600);
  getImei();
  gsm.end();
  koneksi();
}/*--(end setup )---*/

void loop() {  /*----( LOOP: RUNS CONSTANTLY )----*/
  /*----( Procedure: CHECK CONNECTION, SUBSCRIBE THE TOPIC, SAVE THE MESSAGE IN A VARIABLE, WRITE THE MESSAGE IN LCD)----*/
  //MQTT SUBSCRIBE FUNCTION
  if ((mqtt.connected()) && (client.connected())) {
    sprintf(topik,"domain/stuff/%s",getImei());
    ResetWatchdog();
    mqtt.subscribe(topik);
  }else {
    Serial.println(F("disconnect"));
    mqtt.disconnect();
    client.stop();
    koneksi();
  }
  istirahat(10000);
  //Serial.println(F("D2"));
  if (millis()>86400000)  {
    while(1); // agar setiap hari minimal merestart. dan menonaktifkan resetWDT
  }
  mqtt.loop();
}/* --(end main loop )-- */

/* mqtt pubsub and sim900 gprs & imei functions */
char message_buff[100];
void callback(char* topik, byte* payload, unsigned int length) {

  int i = 0;
  Serial.println("Message arrived:  topic: " + String(topik));
  Serial.println("Length: " + String(length,DEC));
  // create character buffer with ending null terminator (string)
  for(i=0; i<length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';
  String msgString = String(message_buff);
  Serial.println("Payload: " + msgString);
  lcd.clear();
  lcd.write(message_buff);
}

void koneksi() {
  client.begin(9600);
  if (!client) {
    Serial.println(F("SIM900 could not be initialized. Check power and baud rate."));
    for (;;);
  }
  while (!client.attach("3data", "3data", "3data")) {
    Serial.println(F("Could not attach GPRS."));
  }
  char willtopic[50];
  char clientID[20];
  sprintf(clientID,"%s",getImei());
  sprintf(willtopic,"status/%s",clientID);
  while (!mqtt.connect(getImei(),willtopic,0,1,"OFF")) {
    Serial.println(F("mqtt gagal"));
  }
  Serial.println(F("connected"));
  char on[] = "ON";
  mqtt.publish(willtopic,(uint8_t*)on,strlen(on),true);
}

void istirahat(unsigned long ms)  {
  unsigned long start = millis();
  while (millis() - start < ms){}
}

char * getImei()  {
  int index = 0;
  char temp;

  line :
  String resp;

  while(gsm.available() > 0){
    temp = gsm.read();
    Serial.print(temp);
  }
  while (awal){
    index=0;
    gsm.println("AT+GSN");
    delay(500);
    while(gsm.available() > 0){
      temp = gsm.read();
      Serial.print(temp);
      if(isdigit(temp)){
        imei[index] = temp;
        index++ ;
        imei[index] = '\0';
      }
    }
    /* if failed to retrieve imei, proceed to "line :" until successfully obtained */
    if(index<10)
    {
      Serial.println(F("Imei Gagal"));
      digitalWrite(onModulePin,HIGH);
      delay(3000);
      digitalWrite(onModulePin,LOW);
      delay(3000);
      goto line;
      delay(200);
    }
    else
    {
      Serial.print(F("Imei Sukses; IMei = "));
      Serial.println(imei);
      awal = false;
      delay(200);
    }
  }
  /* whilist done, every function related to getImei will return to the imei variable because the imei number is saved to this variable (imei) */
  return imei;
}

void ResetWatchdog()  {
  pinMode(9, OUTPUT); //The D0 line goes Low
  istirahat(200);          //Wait for capacitor discharge
  pinMode(9, INPUT);  //The d0 line goes Hi-Z
}
/* ( THE END ) */
